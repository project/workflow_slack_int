INTRODUCTION
------------

Workflow Slack Integration

This simple module uses setting in user settings and in slack setup for 
Drupal to post to a slack channel immediately on changing states in workflow.

A custom workflow state can be defined in the admin settings menu.


REQUIREMENTS
------------

This module requires the folowing modules:

* Workflow (https://www.drupal.org/project/workflow)
* Slack (https://www.drupal.org/project/slack)


INSTALLATION
------------

* Install as you would normally install a contributed Drupal module. 

Visit 
https://www.drupal.org/docs/8/extending-drupal-8/installing-drupal-8-modules 
for further information.


CONFIGURATION
-------------

This module has an admin settings menu that allows you to enable/disable 
the module and define the name of the workflow transition that will 
trigger the Slack messaging. 

If you do not set a workflow name, it will default to the Draft state.

This module relies on settings defined in the Slack module. Slack will 
need to be established and defined before Workflow Slack Integration 
will work properly.

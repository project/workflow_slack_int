<?php

namespace Drupal\workflow_slack_integration\Form;

use Drupal\Core\Form\ConfigFormBase;
use Drupal\Core\Form\FormStateInterface;

/**
 * Provides a 'Workflow Slack Integration Control' Form.
 *
 * @Form(
 *   id = "workflow_slack_int_form",
 *   admin_label = @Translation("Workflow Slack Integration Form"),
 *   category = @Translation("Form Controls Workflow Slack Integration"),
 * )
 */
class WorkflowSlackIntForm extends ConfigFormBase {

  /**
   * {@inheritdoc}
   */
  protected function getEditableConfigNames() {
    return [
      'workflow_slack_int.settings',
    ];
  }

  /**
   * {@inheritdoc}
   */
  public function getFormId() {
    return 'workflow_slack_int_form';
  }

  /**
   * {@inheritdoc}
   */
  public function buildForm(array $form, FormStateInterface $form_state) {
    $config = $this->config('workflow_slack_int.settings');

    $form['workflow_slack_int_enabled'] = [
      '#type' => 'checkbox',
      '#title' => $this->t('Turn on Slack Messaging'),
      '#description' => $this->t('Slack hook will communicate via Slack to below Slack channel and individual users on Workflow transitions.'),
      '#default_value' => $config->get('workflow_slack_int_enabled'),
    ];

    $form['workflow_slack_int_state'] = [
      '#type' => 'text',
      '#title' => $this->t('Set Workflow Message State'),
      '#description' => $this->t('Name of the workflow state that needs approval.'),
      '#default_value' => 'draft',
    ];

    return parent::buildForm($form, $form_state);
  }

  /**
   * {@inheritdoc}
   */
  public function submitForm(array &$form, FormStateInterface $form_state) {
    // $values = $form_state->getValues();
    $this->config('workflow_slack_int.settings')
      ->set('workflow_slack_int_enabled', $form_state->getValue('workflow_slack_int_enabled'))
      ->save();
    parent::submitForm($form, $form_state);
  }

  /**
   * {@inheritdoc}
   */
  public function defaultConfiguration() {
    $default_config = $this->config('workflow_slack_int.settings');
    return [
      'workflow_slack_int_enabled' => $default_config->get('settings.workflow_slack_int_enabled'),
    ];
  }

}
